import { Component, OnInit, Input } from '@angular/core';
import { UserI } from "../shared/user/user";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  @Input() user: UserI;

  constructor() { }

  ngOnInit() {
  }

}
