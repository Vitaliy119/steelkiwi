import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/guard/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  constructor(
    private router: Router,
    public authService: AuthService
  ) { }

  ngOnInit() {}

  logOut (): void {
    localStorage.removeItem('logIn');
    this.router.navigate(['/home']);
    this.authService.logOut();
  }

  logIn(): void {
    this.authService.logIn();
  }
}
