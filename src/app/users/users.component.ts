import { Component, OnInit } from '@angular/core';
import { UserService } from "../shared/user.service";
import { UserI } from "../shared/user/user";

import debounce from "lodash/debounce";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private userService:UserService) { }
  value: string = '';
  users: UserI[] = [];
  divBlock: boolean = true ;
  tableBlock: boolean = false ;

  ngOnInit() { }

  handleChange(): void {
    if (this.value.length) {
      this.userService.getUsers(this.value)
        .subscribe( (data:any) => this.users = data.items)
    } else {
      this.users = []
    }
  }

  inputChange = debounce(this.handleChange, 300);

  clear(): void {
    this.users = [];
    this.value = '';
  }

  changeVersion (el: string): void {
    if (el === 'div') {
      this.divBlock = true;
      this.tableBlock = false;
    } else {
      this.divBlock = false;
      this.tableBlock = true;
    }
  }
}
