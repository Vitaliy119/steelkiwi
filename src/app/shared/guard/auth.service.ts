import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private user: Observable<firebase.User>;
  status: boolean;

  constructor(private firebaseAuth: AngularFireAuth) {
    this.user = firebaseAuth.authState;
    this.isAuth();
  }

  logIn() {
    const provider = new firebase.auth.GithubAuthProvider();
    this.firebaseAuth.auth.signInWithPopup(provider)
      .then((result) => {
        this.status = true;
        localStorage.setItem('logIn', result.additionalUserInfo.username);
      })
      .catch((error) => {
        console.log(error);
        this.status = false;
    });
  }

  logOut() {
      this.status = false;
  }
  isAuth (): boolean {
      if (localStorage.getItem('logIn')) {
          this.status = true;
      }
    return this.status;
  }
}
