import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  getUsers (name) {
    return this.http.get(` https://api.github.com/search/users?per_page=10&amp;q=${name}`);
  }

  getUser (name) {
    return this.http.get(`https://api.github.com/users/${name}`);
  }
}
