import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { UserService } from "../shared/user.service";
import { UserI } from "../shared/user/user";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {
  user: string;
  currentUser: UserI;
  displayInfo: boolean = false;

  constructor(private route: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    this.user = this.route.snapshot.params['name'];
      this.userService.getUser(this.user)
        .subscribe((data:UserI) => {
          this.displayInfo = true;
          this.currentUser = data;
        });
  }

}
