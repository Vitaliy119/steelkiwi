import { Component, OnInit, Input } from '@angular/core';
import { UserI } from "../shared/user/user";

@Component({
  selector: 'app-user-list-table',
  templateUrl: './user-list-table.component.html',
  styleUrls: ['./user-list-table.component.scss']
})
export class UserListTableComponent implements OnInit {

  constructor() { }

  @Input() users: UserI;

  ngOnInit() {
  }

}
